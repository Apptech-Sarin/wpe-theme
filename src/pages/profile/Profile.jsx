import React from "react";
import "./profile.css";
import { Breadcrumb, Input, Row, Col, Form, DatePicker, Button } from "antd";
import { BsPersonCircle, BsHandbag, BsTag } from "react-icons/bs";
import { IoLocationOutline, IoSettings } from "react-icons/io5";
import { useNavigate } from "react-router-dom";

const Profile = () => {
  const navigate = useNavigate();
  return (
    <>
      <section className="Profile">
        <div className="breadcrumbs-p">
          <Breadcrumb>
            <Breadcrumb.Item className="main-crumb">CLILAB</Breadcrumb.Item>
            <Breadcrumb.Item className="sub-crumb">My-Account</Breadcrumb.Item>
          </Breadcrumb>
          <h1>My Account</h1>
        </div>

        <div className="profile-sidebar">
          <h2
            style={{
              marginBottom: "10px",
              background: "#dfdfec",
              width: "10%",
              padding: "5px",
              color: "#13b7ee",
              borderRadius: "5px",
              display: "flex",
            }}
            onClick={() => navigate(`/myProfile`)}
          >
            <div
              style={{
                marginTop: "2px",
              }}
            >
              <BsPersonCircle />
            </div>
            &nbsp; My details
          </h2>
          <h2
            style={{
              marginBottom: "10px",
              display: "flex",
            }}
            onClick={() => navigate(`/addressBook`)}
          >
            <div
              style={{
                marginTop: "2px",
              }}
            >
              <IoLocationOutline />
            </div>
            &nbsp; My address book
          </h2>
          <h2
            style={{
              marginBottom: "10px",
              display: "flex",
            }}
            onClick={() => navigate(`/orders`)}
          >
            <div
              style={{
                marginTop: "2px",
              }}
            >
              <BsHandbag />
            </div>
            &nbsp; My orders
          </h2>
          <h2
            style={{
              marginBottom: "10px",
              display: "flex",
            }}
            onClick={() => navigate(`/newsLetter`)}
          >
            <div
              style={{
                marginTop: "2px",
              }}
            >
              <BsTag />
            </div>
            &nbsp; My newsletters
          </h2>
          <h2
            style={{
              marginBottom: "10px",
              display: "flex",
            }}
            onClick={() => navigate(`/settings`)}
          >
            <div
              style={{
                marginTop: "2px",
              }}
            >
              <IoSettings />
            </div>
            &nbsp; Account settings
          </h2>
        </div>

        <div className="container">
          <div className="account-card">
            <div className="profile-header">
              <h1>My details</h1>
            </div>
            <div className="sub-header">
              <h3>Personal Information</h3>
            </div>
            <hr />

            <Row gutter={40} className="details-row">
              <Col span={10}>
                <p>
                  Assertively utilize adaptive customer service for future-proof
                  platforms. Completely drive optimal markets.
                </p>
              </Col>
              <Col span={14}>
                <div className="form-combine">
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="First Name">
                      <Input />
                    </Form.Item>
                  </Form>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="Second Name">
                      <Input />
                    </Form.Item>
                  </Form>
                </div>

                <div>
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="Birth Date">
                      <DatePicker placeholder="dd/mm/yyyy" />
                    </Form.Item>
                  </Form>
                </div>

                <div>
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="Phone Number">
                      <Input
                        style={{
                          width: "45%",
                        }}
                      />
                    </Form.Item>
                  </Form>
                  <p className="small-text">
                    Keep 10 digit format with no space and dashes
                  </p>
                </div>

                <Button
                  type="primary"
                  style={{
                    width: "20%",
                  }}
                >
                  Save
                </Button>
              </Col>
            </Row>

            <div className="sub-header">
              <h3>Email Address</h3>
            </div>
            <hr />

            <Row gutter={40} className="details-row">
              <Col span={10}>
                <p>
                  Assertively utilize adaptive customer service for future-proof
                  platforms. Completely drive optimal markets.
                </p>
              </Col>
              <Col span={14}>
                <div>
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="Email">
                      <Input
                        style={{
                          width: "60%",
                        }}
                      />
                    </Form.Item>
                  </Form>
                </div>

                <div>
                  <Form layout="vertical" className="profile-form">
                    <Form.Item label="Password">
                      <Input.Password
                        style={{
                          width: "60%",
                        }}
                      />
                    </Form.Item>
                  </Form>
                </div>

                <Button
                  type="primary"
                  style={{
                    width: "20%",
                  }}
                >
                  Save
                </Button>
              </Col>
            </Row>
          </div>
        </div>
      </section>
    </>
  );
};

export default Profile;
