import React, { useState } from "react";
import "./cart.css";
import {
  Breadcrumb,
  Input,
  Row,
  Col,
  Form,
  Badge,
  Button,
  Checkbox,
} from "antd";
import { BsPersonCircle, BsPlusCircle, BsArrowLeft } from "react-icons/bs";
import { FiArrowDown } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import Artboard from "../../resources/images/Artboard.jpeg";
import Bbutton from "./Button";

const Checkout = () => {
  const navigate = useNavigate();
  const [count, setCount] = useState(0);

  let incrementCount = () => {
    setCount(count + 1);
  };

  let decrementCount = () => {
    setCount(count - 1);
  };

  return (
    <>
      <div className="container">
        <div className="breadcrumbs-p">
          <Breadcrumb>
            <Breadcrumb.Item className="sub-crumb">CLILAB</Breadcrumb.Item>
            <Breadcrumb.Item className="sub-crumb">CART</Breadcrumb.Item>
            <Breadcrumb.Item className="main-crumb">
              INFORMATION
            </Breadcrumb.Item>
            <Breadcrumb.Item className="sub-crumb">SHIPPING</Breadcrumb.Item>
            <Breadcrumb.Item className="sub-crumb">PAYMENT</Breadcrumb.Item>
          </Breadcrumb>
        </div>

        <div className="checkout-info">
          <div
            style={{
              marginTop: "8px",
            }}
          >
            <BsPersonCircle
              style={{
                color: "#766a6a",
              }}
            />
          </div>
          &nbsp;
          <h2
            style={{
              marginTop: "5px",
            }}
          >
            Returning customer?
          </h2>
          &nbsp;&nbsp;
          <h2
            style={{
              fontWeight: "500",
              display: "flex",
              marginTop: "5px",
            }}
          >
            Click here to login
            <div
              style={{
                marginTop: "5px",
              }}
            >
              <FiArrowDown
                style={{
                  color: "#4a4545",
                }}
              />
            </div>
          </h2>
        </div>

        <Row gutter={54} className="checkout-row">
          <Col span={14}>
            <h2>Contact information</h2>
            <Form layout="vertical" className="profile-form">
              <Form.Item>
                <Input
                  className="checkout-input"
                  placeholder="Email address *"
                />
              </Form.Item>

              <Form.Item>
                <Checkbox>Create an account?</Checkbox>
              </Form.Item>
            </Form>

            <h2>Shipping address</h2>
            <Form layout="vertical" className="profile-form">
              <div
                style={{
                  display: "flex",
                }}
              >
                <Form.Item>
                  <Input
                    className="checkout-input-p"
                    placeholder="First name *"
                  />
                </Form.Item>
                &nbsp;&nbsp;&nbsp;
                <Form.Item>
                  <Input
                    className="checkout-input-p"
                    placeholder="Last name *"
                  />
                </Form.Item>
              </div>
            </Form>

            <div className="company-attach">
              <div
                style={{
                  marginTop: "1px",
                  opacity: 0.7,
                  marginRight: "5px",
                }}
              >
                <BsPlusCircle />
              </div>
              <h2>Company name (optional)</h2>
            </div>

            <Form layout="vertical" className="profile-form">
              <Form.Item>
                <Input
                  className="checkout-input"
                  placeholder="Country / Region *"
                />
              </Form.Item>
              <Form.Item>
                <Input
                  className="checkout-input"
                  placeholder="House number and street name *"
                />
              </Form.Item>
            </Form>

            <Form layout="vertical" className="profile-form">
              <div
                style={{
                  display: "flex",
                }}
              >
                <Form.Item>
                  <Input
                    className="checkout-input-q"
                    placeholder="Town / City *"
                  />
                </Form.Item>
                &nbsp;&nbsp;&nbsp;
                <Form.Item>
                  <Input
                    className="checkout-input-q"
                    placeholder="State / Zone *"
                  />
                </Form.Item>
                &nbsp;&nbsp;&nbsp;
                <Form.Item>
                  <Input
                    className="checkout-input-q"
                    placeholder="Postcode / ZIP *"
                  />
                </Form.Item>
              </div>
            </Form>

            <Form layout="vertical" className="profile-form">
              <Form.Item>
                <Input className="checkout-input" placeholder="Phone *" />
              </Form.Item>
            </Form>

            <div className="checkout-btn">
              <Button
                style={{
                  border: "none",
                  display: "flex",
                }}
                type="ghost"
                onClick={() => navigate(`/cart`)}
              >
                <div
                  style={{
                    marginTop: "2px",
                    marginRight: "5px",
                  }}
                >
                  <BsArrowLeft />
                </div>
                Back to cart
              </Button>
              <Button type="primary">Continue to Shipping</Button>
            </div>
          </Col>
          <Col className="checkout-pricing" span={10}>
            <div className="checkout-combine">
              <div
                className="product-badge"
                style={{
                  display: "flex",
                }}
              >
                <Badge color="black" count={2}>
                  <img src={Artboard} alt="Artboard" />
                </Badge>
                &nbsp; &nbsp;
                <div>
                  <h2
                    style={{
                      marginLeft: "10px",
                    }}
                  >
                    Artboard
                  </h2>
                  <Row
                    style={{
                      border: "1px solid #796e6e",
                      borderRadius: "10px",
                      height: "30px",
                      marginLeft: "10px",
                      paddingLeft: "5px",
                      width: "80px",
                    }}
                  >
                    <Bbutton title={"-"} action={decrementCount} />

                    <h2
                      style={{
                        borderRight: "1px solid black",
                        borderLeft: "1px solid black",
                        padding: "2px",
                        width: "25px",
                        paddingLeft: "6px",
                      }}
                    >
                      {count}
                    </h2>
                    <Bbutton title={"+"} action={incrementCount} />
                  </Row>
                </div>
              </div>

              <div className="checkout-single">
                <h1>$154.00</h1>
              </div>
            </div>
            <div className="checkout-combine">
              <div
                className="product-badge"
                style={{
                  display: "flex",
                }}
              >
                <Badge color="black" count={1}>
                  <img src={Artboard} alt="Artboard" />
                </Badge>
                &nbsp; &nbsp;
                <div>
                  <h2
                    style={{
                      marginLeft: "10px",
                    }}
                  >
                    Artboard
                  </h2>
                  <Row
                    style={{
                      border: "1px solid #796e6e",
                      borderRadius: "10px",
                      height: "30px",
                      marginLeft: "10px",
                      paddingLeft: "5px",
                      width: "80px",
                    }}
                  >
                    <Bbutton title={"-"} action={decrementCount} />

                    <h2
                      style={{
                        borderRight: "1px solid black",
                        borderLeft: "1px solid black",
                        padding: "2px",
                        width: "25px",
                        paddingLeft: "6px",
                      }}
                    >
                      {count}
                    </h2>
                    <Bbutton title={"+"} action={incrementCount} />
                  </Row>
                </div>
              </div>

              <div className="checkout-single">
                <h1>$77.00</h1>
              </div>
            </div>

            <hr className="checkout-hr" />
            <h3 className="hr-h3">Have a coupon code?</h3>
            <hr className="checkout-hr" />

            <Row className="shipping-row">
              <p>Subtotal</p>
              <p>$231.00</p>
            </Row>
            <Row className="shipping-row">
              <p>Shipping</p>
              <p>Free Shipping</p>
            </Row>
            <Row className="shipping-tots">
              <h2>Total</h2>
              <h2
                style={{
                  color: "#fe316f",
                }}
              >
                $231.00
              </h2>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Checkout;
