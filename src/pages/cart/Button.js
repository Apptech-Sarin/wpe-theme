import React from "react";

export default function Bbutton(props) {
  let { action, title } = props;
  return (
    <button
      style={{
        border: "none",
        height: "0px",
        background: "#ebe9e9",
        fontSize: "16px",
        cursor: "pointer",
        fontWeight: "bold",
      }}
      onClick={action}
    >
      {title}
    </button>
  );
}
