import React, { useState } from "react";
import { Row, Col, Steps, Form, Input, Button, Progress } from "antd";
import "./cart.css";
import { useNavigate } from "react-router-dom";
import Artboard from "../../resources/images/Artboard.jpeg";
import Norton from "../../resources/images/Norton.jpg";
import Google from "../../resources/images/Google.png";
import Rating from "../../resources/images/rating.png";
import { MdClear, MdOutlineCheckCircleOutline } from "react-icons/md";
import Bbutton from "./Button";

const { Step } = Steps;
const Cart = () => {
  const navigate = useNavigate();

  const [count, setCount] = useState(0);

  let incrementCount = () => {
    setCount(count + 1);
  };

  let decrementCount = () => {
    setCount(count - 1);
  };

  return (
    <>
      <section className="Cart">
        <div className="container">
          <div className="cart-header">
            <Steps>
              <Step title="Shopping Cart" description="Manage you item list." />
              <Step
                title="Checkout Details"
                description="Checkout your items list."
              />
              <Step title="Order Complete" description="Review your order." />
            </Steps>
          </div>

          <Row gutter={54}>
            <Col span={14}>
              <Row className="cart-row">
                <h1>PRODUCT</h1>
                <h1>PRICE</h1>
                <h1>QUANTITY</h1>
                <h1>TOTAL</h1>
              </Row>
              <hr />
              <Row className="cart-details">
                <div
                  style={{
                    display: "flex",
                  }}
                >
                  <MdClear
                    style={{
                      marginTop: "8px",
                    }}
                  />
                  <img src={Artboard} alt="Artboard" />
                  <p
                    style={{
                      fontSize: "12px",
                      marginTop: "8px",
                      fontWeight: "500",
                    }}
                  >
                    Artboard
                  </p>
                </div>
                <p
                  style={{
                    marginRight: "50px",
                  }}
                >
                  $77.00
                </p>
                <Row
                  style={{
                    border: "1px solid #796e6e",
                    borderRadius: "10px",
                    height: "30px",
                    marginRight: "50px",
                    paddingLeft: "5px",
                    width: "80px",
                  }}
                >
                  <Bbutton title={"-"} action={decrementCount} />

                  <h2
                    style={{
                      borderRight: "1px solid black",
                      borderLeft: "1px solid black",
                      padding: "2px",
                      width: "25px",
                      paddingLeft: "6px",
                      color: "black",
                    }}
                  >
                    {count}
                  </h2>
                  <Bbutton title={"+"} action={incrementCount} />
                </Row>
                <h2>$154.00</h2>
              </Row>
              <Row className="cart-details">
                <div
                  style={{
                    display: "flex",
                  }}
                >
                  <MdClear
                    style={{
                      marginTop: "8px",
                    }}
                  />
                  <img src={Artboard} alt="Artboard" />
                  <p
                    style={{
                      fontSize: "12px",
                      marginTop: "8px",
                      fontWeight: "500",
                    }}
                  >
                    Artboard
                  </p>
                </div>
                <p
                  style={{
                    marginRight: "50px",
                  }}
                >
                  $77.00
                </p>
                <Row
                  style={{
                    border: "1px solid #796e6e",
                    borderRadius: "10px",
                    height: "30px",
                    marginRight: "50px",
                    paddingLeft: "5px",
                    width: "80px",
                  }}
                >
                  <Bbutton title={"-"} action={decrementCount} />

                  <h2
                    style={{
                      borderRight: "1px solid black",
                      borderLeft: "1px solid black",
                      padding: "2px",
                      width: "25px",
                      paddingLeft: "6px",
                      color: "black",
                    }}
                  >
                    {count}
                  </h2>
                  <Bbutton title={"+"} action={incrementCount} />
                </Row>
                <h2>$154.00</h2>
              </Row>

              <div className="progress">
                <Progress percent={100} />
                <Row className="progress-message">
                  <div
                    style={{
                      marginRight: "5px",
                      color: "#52C41A",
                      fontSize: "16px",
                    }}
                  >
                    <MdOutlineCheckCircleOutline />
                  </div>
                  <p>Congratulations! you've got free shipping.</p>
                </Row>
              </div>
            </Col>
            <Col span={10}>
              <div className="cart-card">
                <h1>CART TOTALS</h1>
                <Row
                  style={{
                    marginTop: "20px",
                    justifyContent: "space-between",
                    marginBottom: "30px",
                  }}
                >
                  <h2>Subtotal</h2>
                  <h2
                    style={{
                      marginRight: "10px",
                    }}
                  >
                    $233.00
                  </h2>
                </Row>

                <Row
                  style={{
                    justifyContent: "space-between",
                    marginBottom: "30px",
                  }}
                >
                  <h2>Shipping</h2>
                  <div>
                    <h2
                      style={{
                        fontWeight: "300",
                      }}
                    >
                      Free Shipping
                    </h2>
                    <h2
                      style={{
                        fontWeight: "300",
                      }}
                    >
                      Shipping to Nepal
                    </h2>
                    <h2
                      style={{
                        fontWeight: "300",
                      }}
                    >
                      Change address
                    </h2>
                  </div>
                </Row>

                <hr />

                <div className="total-sponsor">
                  <Row
                    style={{
                      justifyContent: "space-between",
                      marginTop: "10px",
                    }}
                  >
                    <h2>Total</h2>
                    <h1
                      style={{
                        color: "#fe316f",
                      }}
                    >
                      $233.00
                    </h1>
                  </Row>
                  <Button
                    onClick={() => navigate(`/checkout`)}
                    className="check-btn"
                  >
                    PROCEED TO CHECKOUT
                  </Button>
                </div>

                <Row className="support">
                  <img src={Norton} alt="supporters" />
                  <img src={Google} alt="supporters" />
                  <img src={Norton} alt="supporters" />
                  <img src={Google} alt="supporters" />
                </Row>
              </div>
            </Col>
          </Row>

          <div className="coupon-code">
            <Form layout="vertical" className="profile-form">
              <Row>
                <Form.Item>
                  <Input
                    className="cart-input"
                    placeholder="Enter Coupon Code *"
                  />
                </Form.Item>
                <Button className="coupon-btn">APPLY COUPON</Button>
              </Row>
            </Form>
          </div>

          <div className="review">
            <h1>Reviews From Customers</h1>

            <div className="review-card">
              <Row>
                <Col span={8}>
                  <p>
                    By far more comfortable clothes label I've ever worn.
                    Perfect for long travels.
                  </p>
                  <img src={Rating} alt="rating" />
                  <h2
                    style={{
                      marginTop: "10px",
                    }}
                  >
                    Steve Alia.
                  </h2>
                </Col>
                <Col span={8}>
                  <p>
                    By far more comfortable clothes label I've ever worn.
                    Perfect for long travels.
                  </p>
                  <img src={Rating} alt="rating" />
                  <h2
                    style={{
                      marginTop: "10px",
                    }}
                  >
                    John Cena.
                  </h2>
                </Col>
                <Col span={8}>
                  <p>
                    By far more comfortable clothes label I've ever worn.
                    Perfect for long travels.
                  </p>
                  <img src={Rating} alt="rating" />
                  <h2
                    style={{
                      marginTop: "10px",
                    }}
                  >
                    Samir Shrestha.
                  </h2>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Cart;
