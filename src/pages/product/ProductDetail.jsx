import React from "react";
import "./product.css";
import { Breadcrumb, Row, Col, notification } from "antd";
import Artboard from "../../resources/images/Artboard.jpeg";
import Artboard2 from "../../resources/images/Artboard-2.jpeg";
import Sale from "../../resources/images/sale.png";
import { BsCartPlus } from "react-icons/bs";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import { useNavigate } from "react-router-dom";

const ProductDetail = () => {
  const navigate = useNavigate();
  const openNotification = (placement) => {
    notification.success({
      description: "Arms Blood Pressure Monitor” has been added to your cart.",
      placement,
    });
  };

  return (
    <>
      <section className="productDetail">
        <div className="container">
          <div className="breadcrumbs">
            <button onClick={() => navigate(`/cart`)} className="notify-btn">
              View cart
            </button>
            <Breadcrumb>
              <Breadcrumb.Item className="main-crumb">CliLab</Breadcrumb.Item>
              <Breadcrumb.Item className="main-crumb">Products</Breadcrumb.Item>
              <Breadcrumb.Item className="main-crumb">
                Medical Equipment
              </Breadcrumb.Item>
              <Breadcrumb.Item>Arms Blood Pressure Monitor</Breadcrumb.Item>
            </Breadcrumb>
          </div>

          <Row className="detail-row" gutter={60}>
            <Col span={12}>
              <Carousel
                showArrows={false}
                showStatus={false}
                showIndicators={false}
              >
                <div>
                  <img src={Artboard} alt="sliderImage" />
                </div>
                <div>
                  <img src={Artboard2} alt="sliderImage" />
                </div>
              </Carousel>
            </Col>
            <Col span={12}>
              <div className="detail-header">
                <div className="detail-title">
                  <h1>Arms Blood Pressure Monitor</h1>
                  <img src={Sale} alt="sale" />
                </div>
                <span>
                  <span
                    style={{
                      fontSize: "12px",
                      fontWeight: "600",
                    }}
                  >
                    CATEGORY:
                  </span>
                  &nbsp;
                  <span
                    style={{
                      fontSize: "12px",
                      fontWeight: "600",
                      color: "#029967",
                    }}
                  >
                    MEDICAL EQUIPMENT
                  </span>
                </span>
              </div>
              <hr className="detail-hr" />

              <div className="detail-price">
                <Row>
                  <h2>
                    <sup>$</sup>17.99
                  </h2>
                  &nbsp;&nbsp;
                  <h2 className="detail-cross">
                    <sup>$</sup>19.99
                  </h2>
                </Row>
                <p>Easy-to-read, blue illuminated XL display</p>
              </div>
              <hr className="detail-hr" />
              <div className="detail-cart">
                <Row>
                  <div className="detail-quantity">
                    <p>Arms Blood Pressure Monitor quantity</p>
                  </div>
                  <input
                    type="number"
                    className="detail-input"
                    min="0"
                    max="10"
                    defaultValue={0}
                  />
                </Row>
                <button
                  onClick={() => openNotification("top")}
                  className="detail-btn"
                >
                  <span>
                    <BsCartPlus size={22} />
                  </span>
                  &nbsp;&nbsp;
                  <span>Add to cart</span>
                </button>
              </div>
            </Col>
          </Row>

          <hr className="detail-hr" />
        </div>
        <div className="description">
          <Row className="description-row">
            <h2
              style={{
                cursor: "pointer",
              }}
            >
              Description
            </h2>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h2 className="description-review">Reviews (0)</h2>
          </Row>

          <div className="description-detail">
            <p>
              Upper arm blood pressure monitors measure directly at heart level
              and therefore provide precise measurements. Whether they have
              easy-to-use single-button operation, a stylish black touch sensor
              display or a particularly ergonomic design, our products all have
              one thing in common reliable results for your health.
            </p>
          </div>
        </div>
      </section>
    </>
  );
};

export default ProductDetail;
