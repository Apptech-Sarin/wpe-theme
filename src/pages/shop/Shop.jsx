import React from "react";
import "./shop.css";
import { Breadcrumb, Select } from "antd";
import { MdFilterList } from "react-icons/md";
import FeatureProducts from "../../components/feature-products/FeatureProducts";
import Products from "../../components/products/Product";
import { useNavigate } from "react-router-dom";

const { Option } = Select;
const Shop = () => {
  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  const navigate = useNavigate();
  return (
    <>
      <div className="container">
        <div className="breadcrumbs">
          <Breadcrumb>
            <Breadcrumb.Item className="main-crumb">CliLab</Breadcrumb.Item>
            <Breadcrumb.Item>Products</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className="middle-header">
          <h1>The Clilab Medical Shop</h1>
        </div>

        <div className="featured-products">
          {FeatureProducts.map((fp) => {
            return (
              <>
                <div className="feature-box">
                  <div className="feature-image">
                    <img src={fp.image} alt="feature-product" />
                  </div>
                  <div className="feature-title">
                    <p>{fp.title}</p>
                  </div>
                </div>
              </>
            );
          })}
        </div>
        <hr className="shop-hr" />

        <div className="filter-sort">
          <div className="filterBtn">
            <button className="filter-btn">
              <p>
                <MdFilterList
                  style={{
                    marginRight: "5px",
                    marginTop: "5px",
                  }}
                />
              </p>
              <span>FILTER</span>
            </button>
          </div>
          <div className="sorting">
            <Select
              defaultValue="Default sorting"
              style={{
                width: 180,
              }}
              onChange={handleChange}
              bordered={false}
            >
              <Option value="popularity">Sort by popularity</Option>
              <Option value="rating">Sort by average rating</Option>
            </Select>
            <span className="span-result"></span>
            <span>Showing 1-15 of 16 results</span>
          </div>
        </div>

        <div className="shop-section">
          {Products.map((p) => {
            return (
              <>
                <div
                  className="product-card"
                  onClick={() => navigate(`/shop/products`)}
                >
                  <div className="card-header">
                    <h2>{p.title}</h2>
                    <img src={p.headerImage} alt="sale" />
                  </div>
                  {/* <div className="rating">
                    <img src={p?.rating} alt="rating" />
                  </div> */}
                  <div className="card-img">
                    <img src={p.image} alt="product" />
                  </div>
                  <div className="card-footer">
                    <div className="product-price">
                      <span>{p.price}</span>
                      <p>{p.crossPrice}</p>
                    </div>
                    <div className="hover-back">
                      <div className="hover-icon">
                        <span>{p.cartIcon}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Shop;
