import Dettol from "../../resources/images/dettol.jpg";

const FeatureProducts = [
  {
    id: 1,
    image: Dettol,
    title: "Beauty and Hygiene",
  },
  {
    id: 2,
    image: Dettol,
    title: "Drugstore",
  },
  {
    id: 3,
    image: Dettol,
    title: "Facemask",
  },
  {
    id: 4,
    image: Dettol,
    title: "Medical Equipment",
  },
  {
    id: 5,
    image: Dettol,
    title: "Medicines",
  },
  {
    id: 6,
    image: Dettol,
    title: "Vitamins",
  },
];

export default FeatureProducts;
