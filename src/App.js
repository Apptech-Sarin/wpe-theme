import React from "react";
import "./index.css";
import Home from "./pages/home/Home";
import Shop from "./pages/shop/Shop";
import Cart from "./pages/cart/Cart";
import Checkout from "./pages/cart/Checkout";
import Profile from "./pages/profile/Profile";
import AddressBook from "./pages/profile/AddressBook";
import ProductDetail from "./pages/product/ProductDetail";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AccountSettings from "./pages/profile/AccountSetting";
import MyOrder from "./pages/profile/MyOrder";
import NewsLetter from "./pages/profile/NewsLetter";

const App = () => {
  return (
    <>
      <div>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/shop" exact element={<Shop />} />
            <Route path="/cart" exact element={<Cart />} />
            <Route path="/checkout" exact element={<Checkout />} />
            <Route path="/myProfile" exact element={<Profile />} />
            <Route path="/addressBook" exact element={<AddressBook />} />
            <Route path="/orders" exact element={<MyOrder />} />
            <Route path="/newsLetter" exact element={<NewsLetter />} />
            <Route path="/settings" exact element={<AccountSettings />} />
            <Route path="/shop/products" exact element={<ProductDetail />} />
          </Routes>
        </Router>
      </div>
    </>
  );
};

export default App;
